<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $table = 'events';

    protected $fillable = [
        'summary',
        'description',
        'start_date',
        'end_date'
    ];

    protected $dates = [
        'start_date',
        'end_date'
    ];

    public function getDatesAt8601Attributes()
    {
        return $dates->format('c');
    }
}
