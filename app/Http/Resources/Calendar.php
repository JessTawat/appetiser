<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Calendar extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'id' => $this->id,
            'summary' => $this->summary,
            'description' => $this->description,
            'location' => $this->location,
            'start' => [
                'dateTime' => $this->start_date
            ],
            'end' => [
                'dateTime' => $this->end_date
            ],
            'color' => $this->color
            ];
    }
}
