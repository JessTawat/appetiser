<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calendar;
use App\Http\Resources\Calendar as CalendarResource;

class CalendarController extends Controller
{
    public function index ()
    {
        $events = Calendar::all();

        return CalendarResource::collection($events);
    }

    public function store(Request $request)
    {
        $event = new Calendar;
        $event->summary = $request->input('summary');
        $event->description = $request->input('description');
        $event->color = $request->input('color');
        $event->start_date = $request->input('start.dateTime');
        $event->end_date = $request->input('end.dateTime');

        if ($event->save()) {
            return new CalendarResource($event);
        }
    }
}
