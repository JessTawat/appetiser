import axios from 'axios'

const state = {
  events: []
}

const getters = {
  allEvents: (state) => state.events
}

const actions = {
  fetchEvents ({ commit }) {
    const API_ENDPOINT = process.env.API_ENDPOINT
    axios.get(`${API_ENDPOINT}/api/events`)
      .then(res => {
        commit('FETCH_EVENTS', res.data)
      }).catch(err => {
        console.log(err)
      })
  },

  addEvent ({ commit }, event) {
    const API_ENDPOINT = process.env.API_ENDPOINT
    axios.post(`${API_ENDPOINT}/api/events`, event)
      .then(res => {
        commit('CREATE_EVENT', res.data)
      }).catch(err => {
        console.log(err)
      })
  }
}

const mutations = {
  FETCH_EVENTS (state, events) {
    return state.events = events
  },

  CREATE_EVENT (state, event) {
    state.events.unshift(event)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
